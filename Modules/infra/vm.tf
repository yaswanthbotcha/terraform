resource "vsphere_virtual_machine" "Terraformslave" {
  name                 = "${var.HOSTNAME}_${count.index + 1}"
  num_cpus             = "${var.CPU_COUNT}"
  count                = "${var.VM_COUNT}"
  memory               = "${var.MEMORY_COUNT}"
  datastore_id         = "${data.vsphere_datastore.datastore.id}"
  host_system_id       = "${data.vsphere_host.host.id}"
  resource_pool_id     = "${data.vsphere_resource_pool.pool.id}"
  guest_id             = "${data.vsphere_virtual_machine.template.guest_id}"
  scsi_type            = "${data.vsphere_virtual_machine.template.scsi_type}"

  
  network_interface {
    network_id         = "${data.vsphere_network.network.id}"
  }

  
  disk {
    label              = "${var.HOSTNAME}_${count.index + 1}"
    size               = "${var.DISK_SIZE}"
    thin_provisioned   = false
  }

  clone {
    template_uuid = "${data.vsphere_virtual_machine.template.id}"

    customize {
      linux_options {
        host_name = "${var.HOSTNAME}"
        domain    = "telestream.local"
      }

      network_interface {
        ipv4_address    = "${var.IP_ADDRESS}"
        ipv4_netmask    = 22
        dns_server_list = ["8.8.8.8", "8.8.4.4"]
      }

      ipv4_gateway = "192.168.168.1"
    }
  }
}