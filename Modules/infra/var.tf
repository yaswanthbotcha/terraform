
variable "HOSTNAME" {}
variable "DISK_SIZE"{}
variable "MEMORY_COUNT"{}
variable "CPU_COUNT"{}
variable "VM_COUNT"{}
variable "IP_ADDRESS" {}

data "vsphere_datacenter" "dc" {
  name = "Test"
}

data "vsphere_resource_pool" "pool" {

  name          = "cluster1/Resources"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_host" "host" {
  name          = "192.168.168.155"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_datastore" "datastore" {
  name          = "datastore1"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}


data "vsphere_network" "network" {
  name          = "VM Network"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

data "vsphere_virtual_machine" "template" {
  name          = "mastertemp"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}