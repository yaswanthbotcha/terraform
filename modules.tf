module "infra" {
  source              = "./Modules/infra"
  HOSTNAME            = "${var.HOSTNAME}"
  IP_ADDRESS          = "${var.IP_ADDRESS}"
  CPU_COUNT           = "${var.CPU_COUNT}"
  VM_COUNT            = "${var.VM_COUNT}"
  MEMORY_COUNT        = "${var.MEMORY_COUNT}"
  DISK_SIZE           = "${var.DISK_SIZE}"
}
